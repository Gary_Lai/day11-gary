##  Daily Summary 

**Time: 2023/7/26**

**Author: 赖世龙**

---

**O (Objective):**  Today I learned about router, axios, hooks, etc. I learned how to use router to build a SPA application without refreshing the entire page, I learned how to use axios to call mock apis, and hooks to call requests.

**R (Reflective):**  Satisfied

**I (Interpretive):**  The router is very common and useful in front-end projects, and mastering axios to call back-end apis is also a must-have skill for a full-stack engineer.

**D (Decisional):**  An in-depth study of the use of routers and hooks is an essential step to mastering react and will be practiced more in the future.