import { useDispatch } from "react-redux"
import todoApi from "../apis/todoApi"
import { initTodoList } from "../components/todoSlice"



const useTodos = () => {

  const dispatch = useDispatch()

  const getTodos = async () => {
    const { data } = await todoApi.getTodos()
    dispatch(initTodoList(data))
  }

  const getTodoById = async (id) => {
    const { data } = await todoApi.getTodoById(id)
    return data
  }

  const createTodo = async (text) => {
    await todoApi.createTodo(text)
    getTodos()
  }

  const deleteTodo = async (text) => {
    await todoApi.deleteTodo(text)
    getTodos()
  }

  const updateTodo = async (id, json) => {
    await todoApi.putTodo(id, json)
    getTodos()
  }

  return {
    getTodos,
    createTodo,
    deleteTodo,
    updateTodo,
    getTodoById
  }

}

export default useTodos