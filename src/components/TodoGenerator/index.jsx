import {useState} from 'react'
import useTodos from '../../hooks/useTodos'
import './index.css'
import {Button} from 'antd'

const TodoGenerator = (props) => {
  const [text, setText] = useState('')
  const [loading, setLoading] = useState(false)

  const {createTodo} = useTodos()

  const handleClick = async () => {
    if (text === '') {
      alert('请输入内容')
      return
    }
    setLoading(true)
    await createTodo(text)
    setText('');
    setLoading(false);
  }

  return (
    <div className='generator-container screen'>
      <input value={text} onChange={(e) => setText(e.target.value)} placeholder='enter your todo'
             className='input'/>
      <Button className='add-button margin-left' onClick={handleClick} type='primary' loading={loading}>add
        todo</Button>
    </div>
  )
}

export default TodoGenerator