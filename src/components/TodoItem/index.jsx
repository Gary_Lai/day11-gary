import useTodos from '../../hooks/useTodos';
import {EditOutlined} from '@ant-design/icons';
import {Input, Modal, Popover} from 'antd';
import {useState} from 'react';
import './index.css';

const TodoItem = (props) => {
    const {deleteTodo, updateTodo} = useTodos()
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [text, setText] = useState('');

    const showModal = (e) => {
        setText(props.item.text)
        stopPropagation(e);
        setIsModalOpen(true);
    };
    const handleOk = () => {
        if (text.trim().length === 0) {
            alert('请输入内容')
            return;
        }
        setIsModalOpen(false);
        updateTodo(props.item.id, {text: text, done: props.item.done})
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const stopPropagation = (e) => {
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
    }

    const handleDeleteClick = (e) => {
        stopPropagation(e);
        deleteTodo(props.item.id);
    }

    const handleInput = (e) => {
        setText(e.target.value)
    }

    const handleClickUpdate = () => {
        updateTodo(props.item.id, {done: !props.item.done, text: props.item.text})
    }

    return (
        <div className='todo-item-container screen'>
            <Modal title="Edit Todo Text" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Input placeholder="enter your new todo text" value={text} onChange={handleInput}/>
            </Modal>
            <div className={'todo-item'} onClick={handleClickUpdate}>
                <Popover content={props.item.text}>
                    <div className={props.item.done ? 'done-item item' : 'item'}>{props.item.text}</div>
                </Popover>
                <span className='icon-container'>
                    <div className='edit' onClick={showModal}><EditOutlined/></div>
                    <div className={'delete-icon'} onClick={handleDeleteClick}>&times;</div>
                </span>
            </div>
        </div>
    )
}

export default TodoItem