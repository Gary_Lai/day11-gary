import { useEffect } from 'react'
import useTodos from '../../hooks/useTodos'
import TodoGenerator from '../TodoGenerator'
import TodoGroup from '../TodoGroup'
import './index.css'
const TodoList = () => {

    const {getTodos} = useTodos()

    useEffect(() => {
        getTodos()
    })

    return (
        <div className='list-container screen'>
            <h3>Todo List</h3>
            <TodoGenerator />
            <TodoGroup />
        </div>
    )
}

export default TodoList
