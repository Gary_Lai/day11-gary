import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [],
    },
    reducers: {
        initTodoList: (state, action) => {
            state.todoList = action.payload
        },
        addItem: (state, action) => {
            state.todoList.push({ id: Date.now(), text: action.payload, done: false })
        },
        deleteItem: (state, action) => {
            state.todoList = state.todoList.filter((item) => item.id !== action.payload)
        },
        updateItem: (state, action) => {
            state.todoList.forEach((item) => {
                if (item.id === action.payload) {
                    item.done = !item.done
                }
            })
        },
    },
})

// Action creators are generated for each case reducer function
export const { addItem, deleteItem, updateItem, initTodoList } = todoSlice.actions

export default todoSlice.reducer