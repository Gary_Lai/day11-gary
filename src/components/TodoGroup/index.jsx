import TodoItem from '../TodoItem'
import { useSelector } from 'react-redux'

const TodoGroup = () => {
    const todoList = useSelector((state) => state.todo.todoList)

    return (
        <div>{todoList.map((value) => (<TodoItem key={value.id} item={value}></TodoItem>))}</div>
    )
}

export default TodoGroup