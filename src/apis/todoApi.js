import axios from "axios";
import instance from './apiConfig'


export const getTodos = () => {
  return instance.get('/todos')
}

export const getTodoById = (id) => {
  return instance.get(`/todos/${id}`)
}

export const createTodo = (text) => {
  return instance.post(`/todos`, {
    text,
    done: false
  })
}

export const deleteTodo = (id) => {
  return instance.delete(`/todos/${id}`)
}

export const putTodo = (id, json) => {
  return instance.put(`/todos/${id}`, json)
}


const todoApi = {getTodos, createTodo, deleteTodo, putTodo, getTodoById}

export default todoApi