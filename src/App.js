import { Outlet } from 'react-router-dom';
import './App.css';
import Navigate from './page/Navigate';

function App() {


  return (
    <div className="App">
      <Navigate></Navigate>
      <Outlet></Outlet>
    </div>
  );
}

export default App;
