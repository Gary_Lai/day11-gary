import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './store'
import { Provider } from 'react-redux'
import { createBrowserRouter,
          RouterProvider} from 'react-router-dom';
import NotFound from './page/NotFound';
import TodoList from './components/TodoList';
import Help from './page/Help';
import DoneList from './page/DoneList';
import DoneDetail from './page/DoneDetail';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App></App>,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: "/help",
        element: <Help></Help>
      },
      {
        path: "/doneList",
        element: <DoneList></DoneList>
      },
      {
        path: "/doneList/:id",
        element: <DoneDetail></DoneDetail>
      },
    ]
  },
  {
    path: "*",
    element: <NotFound></NotFound>,
  },
])

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}>
        <App />
      </RouterProvider>
    </Provider>
  </React.StrictMode>
);


reportWebVitals();
