import {Button} from "antd"
import {useNavigate, useParams} from "react-router-dom"
import './index.css'
import useTodos from "../../hooks/useTodos";
import {useEffect, useState} from 'react';

const DoneDetail = () => {
  const {id} = useParams();
  const navigate = useNavigate();
  const {getTodoById} = useTodos();
  const [text, setText] = useState(null);

  useEffect(() => {
    getTodoById(id).then((todo) => {
      if (todo !== null) {
        setText(todo.text);
      }
    })
  }, [id, getTodoById]);

  const handleClick = () => {
    navigate('/doneList');
  };

  return (
    <div className='screen detail-container'>
      <h3>Detail</h3>
      <div className="detail-item">
        <div>id: {id}</div>
        <div>text: {text}</div>
      </div>
      <br/>
      <div className="go-back">
        <Button className='add-button' onClick={handleClick} type='primary'>go back</Button>
      </div>
    </div>
  );
};

export default DoneDetail