import './index.css'

const NotFound = () => {
    return (
        <div className="not-found-container">
            404 NOT FOUND
        </div>
    )
}

export default NotFound