import { Link } from 'react-router-dom';
import { Menu } from 'antd';
import { useState } from 'react';

const Navigate = () => {


  const items = [
    {
      label: (<Link to="/">HOME</Link>),
      key: '1'
    },
    {
      label: (<Link to="/help">HELP</Link>),
      key: '2'
    },
    {
      label: (<Link to="/doneList">DONE</Link>),
      key: '3'
    },
  ]

  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    setCurrent(e.key);
  };


  return (
    <>
      <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} style={{width: '210px'}}/>
    </>
  )
}

export default Navigate