import { useSelector } from "react-redux"
import DoneItem from "../DoneItem"
import './index.css'

const DoneList = () => {
  const todoList = useSelector((state) => state.todo.todoList)

  return (
    <div className="done-list-contaier screen">
      <h3>Done List</h3>
      {todoList.filter(item => item.done === true).map((value) => (<DoneItem key={value.id} item={value}></DoneItem>))}
    </div>
  )
}

export default DoneList