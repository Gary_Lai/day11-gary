import {useNavigate} from "react-router-dom"
import './index.css'

const DoneItem = (props) => {
  const navigate = useNavigate()
  const handleMoveToDetail = (id) => {
    navigate(`/doneList/${id}`)
  }

  return (
    <div className='todo-item' onClick={() => handleMoveToDetail(props.item.id)}>
      <span>{props.item.text}</span>
    </div>
  )
}

export default DoneItem